# TooltipSter List

This plugin is base on [tooltipster plugin](https://github.com/iamceege/tooltipster).

This plugin add search and list functionnalities.

## Screenshot

![screenshot](http://img11.hostingpics.net/pics/527959MenuDmarrer.jpg)

## How to use it

HTML code example :

    <section>
        <button>Tooltip at the bottom</button>
    </section>

And JavaScript code :

    $('button').click(function() {
        $(this).tooltipList({
            availableOptions : [{
                label : "Op 1",
                val : 1
            }, {
                label : "Op 2",
                val : 2
            }],
            liClickEvent : function(liClickValue) {
                alert ('You clicked on a li with value : ' + liClickValue);
            },
            position : $(this).data('target')
        });
    });


When you'll click on the button a tooltip will appear with a list for your options.

### Options

This plugin allows some configurations :

**position** (stirng) | default **top**

Specifies tooltip position.
Available values : top, bottom, right and left

**animation** (string) | default **fade**

Wich animation when tooltip apperas
Available values : fade, grow, swing, slide and fall

**autoClose** (boolean) | default **true**

If the tooltip must be closed when button loses focus

**tooltipClass** (string) | Default empty string

Class to add on the tooltip HTML element

**availableOptions** (array) | Default value **[{ label : 'default option', val : -1 }]**

Avalaible options on the list. Each element must has a label and a val (value).

**liClickEvent** (function) | **By default it just does a console.log**

Your callback when a item list is clicked.

Callback prototype :

    liClickEvent : function(liValue, origin, tooltip) {
        // code
    }

**origin** is the clicked element jquery element, for example your button

**tooltip** is the tooltip jquery element

## Tooltipster

For more options you can check [tooltipster document](http://iamceege.github.io/tooltipster/).
